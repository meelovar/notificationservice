# Сервис уведомлений

## Используемые средства

- Python 3.10
- Django 4.0
- Django Rest Framework 3.13
- Celery
- Redis

## Выполненные дополнительные задания

- подготовить docker-compose для запуска всех сервисов проекта одной командой
  (пункт 3)
- удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать
  некорректные ответы. Необходимо организовать обработку ошибок и откладывание
  запросов при неуспехе для последующей повторной отправки. Задержки в работе
  внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок
  (пункт 9).

## Установка и запуск

1. В терминале перейти в каталог проекта
2. В файле `.env` изменить переменную `API_TOKEN` на необходимую
3. Запустить контейнер командой

```
sudo docker-compose up -d
```

## Эндпоинты

- `http://localhost:8000/` - api проекта
- `http://localhost:8000/clients/` - клиенты
- `http://localhost:8000/mailings/` - рассылки
- `http://localhost:8000/mailings/full_info/` - статистика по всем рассылкам
- `http://localhost:8000/mailings/<pk>/info/` - статистика по конкретной
  рассылке
- `http://localhost:8000/api/messages/` - сообщения

Описание методов в формате OpenAPI находится в
файле [openapi-schema.yml](openapi-schema.yml).
