from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver

from mailing_app.models import Client, Mailing, Message
from mailing_app.tasks import send_message


@receiver(post_save, sender=Mailing, dispatch_uid="create_message")
def create_message(sender, instance, created, **kwargs):
    def get_clients():
        filters = Q(operator_code=instance.operator_code) | Q(tag=instance.tag)

        return Client.objects.filter(filters).all()

    if created:
        clients = get_clients()

        for client in clients:
            message_args = {
                "client_id": client.id,
                "mailing_id": instance.msg_text
            }
            message = Message.objects.create(**message_args)
            data_to_send = {
                "id": message.id,
                "phone": client.phone_number,
                "text": instance.msg_text
            }
            time_data = {
                "expires": instance.end_datetime
            }

            if not instance.need_to_send:
                time_data["eta"] = instance.start_datetime

            send_message.apply_async((data_to_send, client.id, instance.id), **time_data)
