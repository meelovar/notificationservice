from django.core.validators import RegexValidator

phone_validator = RegexValidator(r"^7\d{10}$")
