from rest_framework import serializers

from mailing_app import models


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Mailing
        fields = "__all__"


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = "__all__"


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Message
        fields = "__all__"
