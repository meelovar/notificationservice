import pytz
from django.db import models
from django.utils import timezone

from mailing_app.validators import phone_validator


class Mailing(models.Model):
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    msg_text = models.TextField()
    operator_code = models.CharField(max_length=3, null=True, blank=True)
    tag = models.CharField(max_length=10, null=True, blank=True)

    @property
    def need_to_send(self):
        now = timezone.now()

        return self.start_datetime <= now <= self.end_datetime


class Client(models.Model):
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(max_length=11, validators=[phone_validator])
    operator_code = models.CharField(max_length=3, editable=False)
    tag = models.CharField(max_length=10, null=True, blank=True)
    timezone = models.CharField(max_length=100, choices=TIMEZONES, default="UTC")

    def save(self, **kwargs):
        self.operator_code = self.phone_number[1:4]

        super().save(**kwargs)


class Message(models.Model):
    send_datetime = models.DateTimeField()
    is_sent = models.BooleanField(default=False)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
