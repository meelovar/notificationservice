from django.urls import include, path
from rest_framework import routers

from mailing_app import views

router = routers.DefaultRouter()

router.register("mailings", views.MailingViewSet)
router.register("clients", views.ClientViewSet)
router.register("messages", views.MessageViewSet)

urlpatterns = [
    path("", include(router.urls)),
]
