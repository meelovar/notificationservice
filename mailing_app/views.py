from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from mailing_app.models import Client, Mailing, Message
from mailing_app.serializers import ClientSerializer, MailingSerializer, MessageSerializer


class MailingViewSet(viewsets.ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    @action(detail=True, methods=['get'])
    def info(self, request, pk=None):
        queryset_mailing = Mailing.objects.all()
        get_object_or_404(queryset_mailing, pk=pk)
        queryset = Message.objects.filter(mailing_id=pk).all()
        serializer = MessageSerializer(queryset, many=True)

        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def full_info(self, request):
        total_count = Mailing.objects.count()
        mailing = Mailing.objects.values('id')
        response_data = {
            'mailings_total': total_count,
            'mailings_sent': 0
        }
        result = {}

        for m in mailing:
            res = {
                'total': 0,
                'sent': 0,
                'not_sent': 0
            }

            messages = Message.objects.filter(mailing_id=m['id']).all()
            sent_count = messages.filter(is_sent=True).count()
            not_sent_count = messages.filter(is_sent=False).count()

            res['total'] = len(messages)
            res['sent'] = sent_count
            res['not_sent'] = not_sent_count
            result[m['id']] = res

        response_data['mailings_sent'] = result

        return Response(response_data)


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class MessageViewSet(viewsets.ModelViewSet):
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
