from datetime import datetime

import pytz
import requests

from mailing_app.models import Client, Mailing, Message
from notification_service.celery import app
from notification_service.settings import API_TOKEN, API_URL


@app.task(bind=True, retry_backoff=True)
def send_message(self, client_id, mailing_id, data, url=API_URL, token=API_TOKEN):
    mailing = Mailing.objects.get(pk=mailing_id)
    client = Client.objects.get(pk=client_id)
    timezone = pytz.timezone(client.timezone)
    current_time = datetime.now(timezone)

    if mailing.start_datetime <= current_time <= mailing.end_datetime:
        headers = {
            'Authorization': f'Bearer {token}',
            'Content-Type': 'application/json'
        }

        try:
            requests.post(f"{url}{data['id']}", headers=headers, json=data)
        except requests.exceptions.RequestException as ex:
            print(f"Error when trying to send message {data['id']}")
            raise self.retry(exc=ex)
        else:
            Message.objects.filter(pk=data['id']).update(is_sent=True)
    else:
        half_day = 12 * 60 * 60
        self.retry(countdown=half_day)
