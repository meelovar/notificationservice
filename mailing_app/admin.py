from django.contrib import admin

# Register your models here.
from mailing_app.models import Client, Mailing, Message

admin.site.register((Mailing, Client, Message))
